# serve-html
Stupid HTTP server which adds `.html` to the requested route and looks for that file.  
Useful for serving the `public/` directory from a [static](https://getzola.org/) [site](https://gitlab.com/schnell1/nexa) generator.  
Useful for serving copied websites from [HTTrack](http://www.httrack.com/).

# Status
- abandoned
- incorporated into [renex: Pragmatic HTTP server for file directories, and HTTP method proxy](https://gitlab.com/schnell1/renex) as
    ```sh
    renex --dir .
    renex --dir . /route
    renex --dir . --bind 127.0.0.1 80
    ```

# Usage

- Locally serve current directory with random port and show log info:
    ```sh
    serve-html --log
    ```
- Publicly serve the output of a static site generator:
    ```sh
    sudo serve-html public/ --ip 0.0.0.0 --port 80
    ```
- Serve directory knobbsi/ with localhost as IPv6 with random port:
    ```sh
    serve-html knobbsi/ --ip ::1
    ```
- Serve directory malware/ on port 7171 in a local network and
  store log info in file access.log:
    ```sh
    serve-html \
        malware/ \
        --ip 192.168.10.5 \
        --port 7171 \
        --log \
        2>> access.log
    ```

# Installation
- clone this repo: `git clone https://gitlab.com/schnell1/serve-html`
- `cd serve-html` into the repo
- `cargo install --path .`
- have `~/.cargo/bin` in your `PATH` environmental variable, e.g.
    ```
    # .profile
    PATH=~/.local/bin:~/.cargo/bin:$PATH
    ```

# License
Any at your option.
