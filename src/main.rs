//! HTTP server for current directory which removes ".html" from the URL.

use easy_error::{bail, Error, ResultExt};
use http::{Request, Response, StatusCode};
use hyper::{
    service::{make_service_fn, service_fn},
    Body, Server,
};
use indoc::indoc;
use log::{info, warn};
use new_mime_guess as mime_guess;
use rand::{
    distributions::{Distribution, Uniform},
    thread_rng,
};
use relative_path::RelativePathBuf;
use std::{
    convert::Infallible,
    env::{self},
    fs::File,
    io::Read,
    net::{IpAddr, SocketAddr},
    path::{Path, PathBuf},
    str::FromStr,
    sync::Arc,
};
use tokio::sync::RwLock as RwLockFuture;

use urlencoding::decode;

/// Adding these to the requested route and trying to find the file.
static HTML_EXT: &[&str] = &[".html", ".htm", "/index.html", "/index.htm"];

async fn serve_dir(
    req: Request<Body>,
    dir: Arc<RwLockFuture<PathBuf>>,
) -> http::Result<Response<Body>> {
    let mut response = Response::builder();
    let req_headers = req.headers();
    let mut body = Body::empty();

    // Make fun of user agents.
    if req_headers.contains_key("User-Agent") {
        response = response.header("server", req_headers["User-Agent"].clone());
    } else {
        response = response.header("server", "coolSchnell()");
    }

    // Try to find file and base files ending with ".html" or one of the other extensions.
    let uri_path = String::from(decode(req.uri().path()).unwrap());
    let root_dir = &*dir.read().await;
    let mut path = RelativePathBuf::new();
    let mut is_html_rewrite = false;
    for ext in HTML_EXT {
        let html_path = RelativePathBuf::from(uri_path.to_owned() + ext);
        if html_path.to_path(&root_dir).exists() {
            path = html_path;
            is_html_rewrite = true;
            break;
        }
    }

    if !is_html_rewrite {
        path = RelativePathBuf::from(uri_path.clone());
    }

    let path = path.to_path(&root_dir);
    #[allow(unused_assignments)]
    let mut status = StatusCode::NOT_FOUND;
    let mut mime = None;

    if path.exists() {
        let file = File::open(path.clone());
        let mut file_content = String::new();
        mime = Some(mime_guess::from_path(path).first_or_octet_stream());

        match file {
            Ok(mut file) => match file.read_to_string(&mut file_content) {
                Ok(_) => {
                    body = Body::from(file_content);
                    response =
                        response.header("Content-Type", mime.as_ref().unwrap().essence_str());
                    status = StatusCode::OK;
                }
                Err(_) => status = StatusCode::FORBIDDEN,
            },
            Err(_) => {
                status = StatusCode::FORBIDDEN;
            }
        }
    } else {
        status = StatusCode::NOT_FOUND;
    }

    let mime = match mime {
        Some(mime) => String::from(mime.essence_str()),
        None => String::from("?"),
    };

    let info = format!(
        "[RESPONSE] URI {uri}, STATUS {status}, MIME {mime}, IS_HTML_REWRITE {is_html_rewrite}",
        status = status,
        mime = mime,
        uri = uri_path,
        is_html_rewrite = is_html_rewrite
    );
    if status == StatusCode::OK {
        info!("{}", info);
    } else {
        warn!("{}", info);
    }
    response.status(status).body(body)
}

fn cli() -> clap::App<'static> {
    use clap::{App, Arg};
    App::new("serve-html")
    .about(indoc! {"
        Stupid HTTP server which adds
            .html
            .htm
            /index.html
            /index.htm
        to the requested route and looks for that file.
    "})
    .override_usage(indoc!{"
        serve-html [DIR] [--ip ADDRESS] [--port PORT] [--log]

            EXAMPLES:
                Locally serve current directory with random port and show log info:
                    serve-html --log
                
                Publicly serve the output of a static site generator:
                    sudo serve-html public/ --ip 0.0.0.0 --port 80
                
                Serve directory knobbsi/ with localhost as IPv6 with random port:
                    serve-html knobbsi/ --ip ::1
                
                Serve directory malware/ on port 7171 in a local network and
                store log info in file access.log:
                    serve-html \\
                        malware/ \\
                        --ip 192.168.10.5 \\
                        --port 7171 \\
                        --log \\
                        2>> access.log"
    })
    .arg(Arg::new("dir")
        .value_name("DIR")
        .index(1)
        .about("Root directory to serve files from.\nIf omitted, current directory is used.")
    )
    .arg(Arg::new("port")
        .value_name("PORT")
        .long("port")
        .short('p')
        .takes_value(true)
        .required(false)
        .about("Connects the service to the given PORT.\n[default: random port between 49152 and 65535]")
    )
    .arg(Arg::new("ip")
        .value_name("ADDRESS")
        .long("ip")
        .short('i')
        .takes_value(true)
        .required(false)
        .about("Connects the service to the given IP.")
        .default_value("127.0.0.1")
    )
    .arg(Arg::new("log")
        .long("log")
        .short('l')
        .takes_value(false)
        .required(false)
        .about("Show logging information while running and write it to stderr.")
    )
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let mut rng = thread_rng();
    let mut ip = IpAddr::from([127, 0, 0, 1]);
    let mut port = Uniform::from(49152..65535u16).sample(&mut rng);
    let mut dir = Path::new("./");

    let args = cli().get_matches();

    if let Some(dir_arg) = args.value_of("dir") {
        let dir_arg = Path::new(dir_arg);
        if dir_arg.exists() {
            dir = dir_arg;
        } else {
            bail!("Directory \"{}\" does not exist.", dir_arg.display())
        }
    }

    if let Some(ip_arg) = args.value_of("ip") {
        if let Ok(ip_parsed) = IpAddr::from_str(ip_arg) {
            ip = ip_parsed;
        } else {
            bail!("\"{}\" is not a valid IP address.", ip_arg);
        }
    }

    if let Some(port_arg) = args.value_of("port") {
        if let Ok(port_arg) = u16::from_str(port_arg) {
            port = port_arg;
        } else {
            bail!("\"{}\" is not a valid port.", port_arg);
        }
    }

    if args.is_present("log") {
        env::set_var("RUST_LOG", "debug");
        pretty_env_logger::init();
    }

    let addr = SocketAddr::new(ip, port);
    info!(
        "[SERVING] {dir} on {addr}",
        dir = dir.canonicalize().unwrap().display(),
        addr = addr
    );

    println!("http://{}/", addr);

    let make_service = make_service_fn(move |_conn| {
        let dir = Arc::new(RwLockFuture::new(PathBuf::from(&dir)));
        async move { Ok::<_, Infallible>(service_fn(move |req| serve_dir(req, dir.clone()))) }
    });
    let server = Server::bind(&addr).serve(make_service);
    server.await.context("Server did not finish successfully.")
}
